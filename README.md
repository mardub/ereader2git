# Publish notes from my ereader
Tested on a MacOS should work on linux too.

1) On your Koreader export all your library notes to html.
2) Fork and clone this folder on your computer
3) Connect your reader device with usb
4) Open render.sh and adapt the file path to access your notes.
5) run in a terminal
```bash
cd path/to/ereader2git
sh render.sh
```
Your notes will be pushed to 
https://{gitusername}.gitlab.io/ereader2git/

See for example:
[https://mardub.gitlab.io/ereader2git/](https://mardub.gitlab.io/ereader2git/)

# Problems with commit?
Try this command:
```
git config --global credential.helper 'cache --timeout=3600000'

```

# Extra for direct upload
I added the "html.lua" in the repository. To use it I replace the file /Volumes/PB632/applications/koreader/plugins/exporter.koplugin/target/html.lua (when the pocket book is connected to my mac).
Note The lines:
```bash
    local reqbody_data = {
        branch = "master",
        author_email = "pyou.pyou@laposte.net",
        author_name = "mardub",
        content = content,
        commit_message = "sent from tablet"
    }

    local reqbody = JSON.encode(reqbody_data)

    request.url = "https://gitlab.com/api/v4/projects/40430643/repository/files/public%2Findex%2Ehtml"
    request.method = "PUT"
    request.headers = {
        ["PRIVATE-TOKEN"] = "glpat--3-531gaMsify0v3m5J6",
        ["Content-Type"] = "application/json",
        ["content-length"] = #reqbody
    }

```
Must have properly replaced credential (the PRIVATE-TOKEN) which must be generated in gitlab with api and write to file authorisation

# Credits
http://uppsala.ai