--OBS REPLACE THE ["PRIVATE-TOKEN"] = "glpat--3-531gaMsify0v3m5J6" by a token valid
local Device = require("device")
local logger = require("logger")
local slt2 = require("template/slt2")
--local http = require("ssl.https")
local http = require("socket.http")
local socket = require("socket")
local socketutil = require("socketutil")
local ltn12 = require("ltn12")
local JSON = require("json")

-- html exporter
local HtmlExporter = require("base"):new {
    name = "html",
    shareable = Device:canShareText(),
}

local function format(booknotes)
    local chapters = {}
    local curr_chapter = nil
    for _, booknote in ipairs(booknotes) do
        if curr_chapter == nil then
            curr_chapter = {
                title = booknote[1].chapter,
                entries = {}
            }
        elseif curr_chapter.title ~= booknote[1].chapter then
            table.insert(chapters, curr_chapter)
            curr_chapter = {
                title = booknote[1].chapter,
                entries = {}
            }
        end
        table.insert(curr_chapter.entries, booknote[1])
    end
    if curr_chapter ~= nil then
        table.insert(chapters, curr_chapter)
    end
    booknotes.chapters = chapters
    booknotes.entries = nil
    return booknotes
end

function HtmlExporter:getRenderedContent(t)
    local title
    if #t == 1 then
        title = t[1].title
    else
        title = "All Books"
    end
    local template = slt2.loadfile(self.path .. "/template/note.tpl")
    local clipplings = {}
    for _, booknotes in ipairs(t) do
        table.insert(clipplings, format(booknotes))
    end
    local content = slt2.render(template, {
        clippings=clipplings,
        document_title = title,
        version = self:getVersion(),
        timestamp = self:getTimeStamp(),
        logger = logger
    })
    return content
end

function HtmlExporter:export(t)
    local path = self:getFilePath(t)
    local file = io.open(path, "w")
    if not file then return false end
    local content = self:getRenderedContent(t)
    file:write(content)
    file:close()

    local sink = {}
    local request = {}

    local reqbody_data = {
        branch = "master",
        author_email = "pyou.pyou@laposte.net",
        author_name = "mardub",
        content = content,
        commit_message = "sent from tablet"
    }

    local reqbody = JSON.encode(reqbody_data)

    request.url = "https://gitlab.com/api/v4/projects/40430643/repository/files/public%2Findex%2Ehtml"
    request.method = "PUT"
    request.headers = {
        ["PRIVATE-TOKEN"] = "glpat--3-531gaMsify0v3m5J6",
        ["Content-Type"] = "application/json",
        ["content-length"] = #reqbody
    }
    request.source = ltn12.source.string(reqbody)
    request.sink = ltn12.sink.table(sink),
    
    socketutil:set_timeout(socketutil.LARGE_BLOCK_TIMEOUT, socketutil.LARGE_TOTAL_TIMEOUT)
    local headers_request = socket.skip(1, http.request(request))
    socketutil:reset_timeout()    

    --local file2 = io.open(path .. "_log", "w")
    --file2:write(table.concat(sink))
    --file2:write(reqbody)
    --file2:close()
    
    return true
end

function HtmlExporter:share(t)
    local content = self:getRenderedContent({t})
    Device:doShareText(content)
end

return HtmlExporter
