#copy files to ereader2git site (adapt path to your device)
#cp path/to/your/device/notefolder parth/to/your/public/gitlabfolder
cp /media/marie/PB632/applications/koreader/clipboard/*all-books.html public
#get the most recent file to return it as the index
mostRecent=$(ls public | grep 2 | tail -1)
cp public/$mostRecent public/index.html
git add public/*
git commit -m "update website of notes"
git push
echo Your site is accessible under https://{gitusername}.gitlab.io/ereader2git/
echo for instance: https://mardub.gitlab.io/ereader2git/
